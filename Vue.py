#-*- coding: utf-8 -*-
from tkinter import *
import helper
import os
from winsound import *
from PIL import ImageTk, Image


class Vue:
    def __init__(self,parent):
        self.parent=parent
        self.mod=self.parent.mod
        self.root=Tk()
        self.root.resizable(width=False,height=False)
        self.root.title("Land Of Conqueror")
        self.root.iconbitmap("assets/Shield-Royal-Swords-icon.ico")
        self.fenetreLargeur=800
        self.fenetreHauteur=700
        self.largeurTerrain=800
        self.hauteurTerrain=600

        self.frameMenu = Frame(self.root)
        self.frameMenu.pack()

        self.frameCanevas = Frame(self.root)
        self.frameCanevas.pack()
        
        self.frameBouton = Frame(self.root)
        self.frameBouton.pack()
        
        self.frameLabels = Frame(self.frameBouton)
        self.frameLabels.pack()
        
        self.frameBoutonMenu = Frame(self.frameBouton)
        self.frameBoutonMenu.pack()
        
        self.menu=Canvas(self.frameMenu,width=self.fenetreLargeur,height=self.fenetreHauteur,bg="darkblue")
        self.menu.pack()
        
        self.canevas=Canvas(self.frameCanevas,width=self.largeurTerrain, height=self.hauteurTerrain, bg="green")
        self.canevas.pack()

        #Globals
        self.currentTag=None
        self.toursUI=[]
        self.toursTags=[]
        self.choixTour=ChoixTour(self.canevas)
        
        self.imgTourArcher=ImageTk.PhotoImage(Image.open("assets/tourArcher.gif"))
        self.imgTourMagic=ImageTk.PhotoImage(Image.open("assets/tourMagic.gif"))
        self.imgTourLazer=ImageTk.PhotoImage(Image.open("assets/tourLazer.gif"))
        self.imgTourBomb=ImageTk.PhotoImage(Image.open("assets/tourBomb.gif"))

        self.imgBackgroundMenu=ImageTk.PhotoImage(Image.open("assets/menuBackground.jpg"))
        self.imgBackgroundMap1=ImageTk.PhotoImage(Image.open("assets/nextlvlbackground.jpg"))
        self.imgBackgroundMap2=ImageTk.PhotoImage(Image.open("assets/backgroundMap2.jpg"))

        self.imgHero=ImageTk.PhotoImage(Image.open("assets/hero.gif"))
        self.imgHeroActive=ImageTk.PhotoImage(Image.open("assets/heroActive.gif"))

        self.imgMinion=ImageTk.PhotoImage(Image.open("assets/minion.gif"))
        
        self.imgPBombarde=ImageTk.PhotoImage(Image.open("assets/bombP.gif"))
        self.imgPMagique=ImageTk.PhotoImage(Image.open("assets/magicP.gif"))
        self.imgPArcher=ImageTk.PhotoImage(Image.open("assets/arrow.gif"))
        
        self.creerMenu()
        self.affichermap1()
        self.creerBoutons()
        
        #Les events
        self.menu.bind("<Button-1>", self.menuClick)
        self.canevas.bind("<Button-1>", self.onleftclick)
        self.canevas.bind("<Button-3>", self.onrightclick)

    def affichermenu(self):
        self.play()
        self.frameMenu.pack()
        self.frameCanevas.pack_forget()
        self.frameBouton.pack_forget()
        self.parent.gameIsRunning=0

    def afficherterrain(self):
        self.stop_sound()
        self.frameCanevas.pack()
        self.frameBouton.pack()
        self.frameMenu.pack_forget()
        self.parent.gameIsRunning=1
        self.parent.jouercoup()

    def afficheEtatDuJeu(self):
        self.canevas.delete("oval")
        self.canevas.delete("hero")
        self.canevas.delete("projectile")

        t=10
        for i in self.mod.creepsSorti:
            oval = self.canevas.create_image(i.x, i.y,image=self.imgMinion, tag="oval")
            barNoir = self.canevas.create_line(i.x-t,i.y-10,i.x+t,i.y-10, fill="black", width=3, tag="oval")
            barVert = self.canevas.create_line(i.x-t,i.y-10,i.x-t+(2*t*i.pourcentageVie),i.y-10, fill="lightgreen", width=3, tag="oval")

        for p in self.mod.projectiles:
            if p.tower.typeDeTour =="LazerBeam":
                projectile=self.canevas.create_line(p.tower.posX, p.tower.posY, p.target.x, p.target.y,fill="pink", width = 3, tag="projectile")
            elif p.tower.typeDeTour=="Bombarde":
                projectile=self.canevas.create_image(p.x, p.y,image=self.imgPBombarde, tag="projectile")
            elif p.tower.typeDeTour=="Magique":
                projectile=self.canevas.create_image(p.x, p.y,image=self.imgPMagique, tag="projectile")
            elif p.tower.typeDeTour=="Archer":
                projectile=self.canevas.create_image(p.x, p.y,image=self.imgPArcher, tag="projectile")  
        
        for h in self.mod.hero:
            if self.mod.hero[0].isSelected == 1:
                self.hero=self.canevas.create_image(h.x,h.y,image=self.imgHeroActive, tag="hero")
            else:
                self.hero=self.canevas.create_image(h.x,h.y,image=self.imgHero, tag="hero")

        #modification des labels
        self.varvie.set("Vie : "+str(self.mod.hp))
        self.varcash.set("Argent : "+str(self.mod.cash))
        self.varvague.set("Vague : "+str(self.mod.nbVagues))

    def affichermap1(self):

        self.canevas.create_image(400,300, image = self.imgBackgroundMap1)
        imgChateau=ImageTk.PhotoImage(Image.open("assets/castle1.gif"))
        turn=5      
            
        imageChateau=self.canevas.create_image(700,560,image=imgChateau)
        self.mod.map.image=imgChateau
    
    def affichermap2(self):
        imgChateau2=ImageTk.PhotoImage(Image.open("assets/castle2.gif"))
        self.canevas.create_image(400,300, image = self.imgBackgroundMap2)
        
        imageChateau2=self.canevas.create_image(720,560,image=imgChateau2)
        self.mod.map.image=imgChateau2
    
    def onleftclick(self,evt):
        tags=self.canevas.gettags(CURRENT)

        if tags != ():

            #print(tags[0])
            self.currentTag=tags[0]

            #Hero
            if(tags[0] == "hero"):
                    self.mod.hero[0].isSelected = 1
            #Tours
            for i in self.toursTags:
                if tags[0] == i:
                    if self.parent.gameIsRunning ==1 and self.mod.cash >= 70:
                        for t in self.mod.tours:
                            if t.idTour == tags[0]:
                                if self.choixTour.isEnabled == 0:
                                    self.choixTour.tourTmp = t
                                    self.choixTour.afficheChoix(t)
                                    self.choixTour.isEnabled = 1

                                elif self.choixTour.isEnabled == 1:
                                    self.choixTour.supprimeChoix(t)
                                    self.choixTour.isEnabled = 0

            if tags[0] == "imgArcher":
                posX = self.choixTour.tourTmp.posX
                posY = self.choixTour.tourTmp.posY
                idTour = self.choixTour.tourTmp.idTour

                self.mod.tours.remove(self.choixTour.tourTmp)
                self.mod.addTower("Archer", 0, posX, posY,idTour)
                self.tourUI(self.choixTour.tourTmp)
                self.choixTour.supprimeChoix(self.choixTour.tourTmp)
                self.choixTour.isEnabled = 0
                self.canevas.itemconfigure(self.canevas.find_withtag(self.choixTour.tourTmp.idTour), state='hidden')
            
            elif tags[0] == "imgBomb":

                posX = self.choixTour.tourTmp.posX
                posY = self.choixTour.tourTmp.posY
                idTour = self.choixTour.tourTmp.idTour

                self.mod.tours.remove(self.choixTour.tourTmp)
                self.mod.addTower("Bombarde", 0, posX, posY,idTour)

                self.tourUI(self.choixTour.tourTmp)
                self.choixTour.supprimeChoix(self.choixTour.tourTmp)
                self.choixTour.isEnabled = 0
                self.canevas.itemconfigure(self.canevas.find_withtag(self.choixTour.tourTmp.idTour), state='hidden')
                
            elif tags[0] == "imgLazer":
                posX = self.choixTour.tourTmp.posX
                posY = self.choixTour.tourTmp.posY
                idTour = self.choixTour.tourTmp.idTour

                self.mod.tours.remove(self.choixTour.tourTmp)
                self.mod.addTower("LazerBeam", 0, posX, posY,idTour)

                self.tourUI(self.choixTour.tourTmp)
                self.choixTour.supprimeChoix(self.choixTour.tourTmp)
                self.choixTour.isEnabled = 0
                self.canevas.itemconfigure(self.canevas.find_withtag(self.choixTour.tourTmp.idTour), state='hidden')
                
            elif tags[0] == "imgMagic":

                posX = self.choixTour.tourTmp.posX
                posY = self.choixTour.tourTmp.posY
                idTour = self.choixTour.tourTmp.idTour

                self.mod.tours.remove(self.choixTour.tourTmp)
                self.mod.addTower("Magique", 0, posX, posY,idTour)

                self.tourUI(self.choixTour.tourTmp)
                self.choixTour.supprimeChoix(self.choixTour.tourTmp)
                self.choixTour.isEnabled = 0
                self.canevas.itemconfigure(self.canevas.find_withtag(self.choixTour.tourTmp.idTour), state='hidden')

        if self.mod.hero[0].isSelected == 1:
            self.deplaceHero(evt)

        if self.mod.hp == 0:
            self.gameoverClick(evt);

        if self.mod.nombreDeCreepMort >= 65:
            self.victoryClick(evt)

    def onrightclick(self,evt):
        self.mod.hero[0].isSelected = 0

    def deplaceHero(self,evt):
        cibleX=evt.x
        cibleY=evt.y
        self.parent.mod.deplaceHero(cibleX,cibleY,"deplace")

    def placerTour(self):
        self.toursTags=[]
        self.diametre=36
        j=0
        for i in self.mod.map.terrainReserveePourTourXY:
            self.canevas.create_rectangle(i[0]-self.diametre/2, i[1]-self.diametre/2, i[0]+self.diametre/2, i[1]+self.diametre/2, fill="lightgreen", width=0, tags="tour"+str(j))
            self.toursTags.append("tour"+str(j))
            j+=1

    def tourUI(self, t):
        tags=self.canevas.gettags(CURRENT)

        if tags != ():
            self.mod.cash-=self.mod.tours[0].cout
            
            if tags[0] == "imgArcher":
                self.canevas.create_image(t.posX,t.posY, image = self.imgTourArcher, tag=self.choixTour.tourTmp.idTour+"-Archer")
            elif tags[0] == "imgMagic":
                self.canevas.create_image(t.posX,t.posY, image = self.imgTourMagic, tag=self.choixTour.tourTmp.idTour+"-Magic")
            elif tags[0] == "imgLazer":
                self.canevas.create_image(t.posX,t.posY, image = self.imgTourLazer, tag=self.choixTour.tourTmp.idTour+"-Lazer")
            elif tags[0] == "imgBomb":
                self.canevas.create_image(t.posX,t.posY, image = self.imgTourBomb, tag=self.choixTour.tourTmp.idTour+"-Bombarde")
            
            """
            self.canevas.create_oval(t.posX-(t.collision/2),
                                    t.posY-(t.collision/2),
                                    t.posX+(t.collision/2),
                                    t.posY+(t.collision/2),
                                    fill="", width=1)
            """

    def menuVictory(self):
        self.boutonVictoryTitre=BoutonArtisanal(self, 750, 100, "", None)
        self.boutonVictoryTitre.create_button_box(self.canevas)
        self.boutonVictoryTitre.center_button(self.canevas, 150)
        self.boutonVictoryTitre.create_normal_text(self.canevas, "Victory", 60, 'black', "victory")

        self.boutonVictoryContinuer=BoutonArtisanal(self, 115, 40, "", None)
        self.boutonVictoryContinuer.create_button_box(self.canevas)
        self.boutonVictoryContinuer.center_button(self.canevas, 300)
        self.boutonVictoryContinuer.create_button_text(self.canevas, "Continuer",'black', "victory")

    def menuGameOver(self):
        self.boutonGameoverTitre=BoutonArtisanal(self, 750, 100, "", None)
        self.boutonGameoverTitre.create_button_box(self.canevas)
        self.boutonGameoverTitre.center_button(self.canevas, 150)
        self.boutonGameoverTitre.create_normal_text(self.canevas, "Game Over", 60, 'black', "gameover")

        self.boutonGameoverRejouer=BoutonArtisanal(self, 115, 40, "", None)
        self.boutonGameoverRejouer.create_button_box(self.canevas)
        self.boutonGameoverRejouer.center_button(self.canevas, 300)
        self.boutonGameoverRejouer.create_button_text(self.canevas, "Rejouer",'black', "gameover")

        self.boutonGameoverQuitter=BoutonArtisanal(self, 115, 40, "", None)
        self.boutonGameoverQuitter.create_button_box(self.canevas)
        self.boutonGameoverQuitter.center_button(self.canevas, 375)
        self.boutonGameoverQuitter.create_button_text(self.canevas, "Quitter",'black', "gameover")

    def creerBoutons(self): 
        #afficher vie (label)
        self.varvie=StringVar()
        self.varvie.set("Vie : "+str(self.mod.hp))
        self.labvie=Label(self.frameLabels,textvariable=self.varvie)
        self.labvie.pack(side=RIGHT)
        
        #afficher argent (label)
        self.varcash=StringVar()
        self.varcash.set("Argent : "+str(self.mod.cash))
        self.labcash=Label(self.frameLabels,textvariable=self.varcash)
        self.labcash.pack(side=RIGHT)
        
        #afficher vague (label)
        self.varvague=StringVar()
        self.varvague.set("Vague : "+str(self.mod.nbVagues))
        self.labvague=Label(self.frameLabels,textvariable=self.varvague)
        self.labvague.pack(side=RIGHT)
        
        #Bouton Menu
        self.boutonMenu=Button(self.frameBoutonMenu, text="Menu", height=4, width=10)
        self.boutonMenu.pack(side=LEFT)
        self.boutonMenu.config(command=self.affichermenu)
        
        #Bouton Pause
        self.boutonPause=Button(self.frameBoutonMenu, text="Pause", height=4, width=10)
        self.boutonPause.pack(side=LEFT)
        self.boutonPause.config(command=self.parent.pauseGame)
        
        #Bouton Sell
        self.boutonSell=Button(self.frameBoutonMenu, text="Vendre", height=3, width=7)
        self.boutonSell.pack(side=RIGHT)
        self.boutonSell.config(command=self.sellTower)
        
        
        #Bouton Améliorer
        self.boutonUpgrade=Button(self.frameBoutonMenu, text="Améliorer", height=3, width=7)
        self.boutonUpgrade.pack(side=RIGHT)
        self.boutonUpgrade.config(command=self.upgradeTower)
    
    def sellTower(self):

        value =  re.match('^[a-z]{4}[0-9]+',self.currentTag)

        self.canevas.itemconfigure(self.currentTag, state='hidden')

        if(value != None):
            self.canevas.itemconfigure(value[0], state='normal')
            self.mod.sellTower(value[0])

    def upgradeTower(self):
        value =  re.match('^[a-z]{4}[0-9]+',self.currentTag)
        if(value != None):
            self.mod.ameliorerTower(value[0])

    def play(self):
        PlaySound('assets/menuMusic.wav', SND_ALIAS | SND_LOOP | SND_ASYNC)

    def stop_sound(self):
        PlaySound(None, SND_FILENAME)

    def creerMenu(self):
        self.menu.create_image(300,300, image = self.imgBackgroundMenu)
        self.boutonMenuTitre=BoutonArtisanal(self, 750, 100, "", None)
        self.boutonMenuTitre.create_button_box(self.menu)
        self.boutonMenuTitre.center_button(self.menu, 100)
        self.boutonMenuTitre.create_normal_text(self.menu, "Land of Conqueror", 60, 'white', "")

        self.boutonMenuJouer=BoutonArtisanal(self, 115, 40, "", None)
        self.boutonMenuJouer.create_button_box(self.menu)
        self.boutonMenuJouer.center_button(self.menu, 290)
        self.boutonMenuJouer.create_button_text(self.menu, "Jouer",'black','')

        self.boutonMenuQuitter=BoutonArtisanal(self, 170, 40, "", None)
        self.boutonMenuQuitter.create_button_box(self.menu)
        self.boutonMenuQuitter.center_button(self.menu, 360)
        self.boutonMenuQuitter.create_button_text(self.menu, "Quitter", 'black', "")

    def victoryClick(self, event):
        self.boutonVictoryContinuer.checkIfButtonIsClicked(event)

        if self.boutonVictoryContinuer.isClicked == 1:
            self.parent.continuer()
            self.boutonVictoryContinuer.isClicked = 0

    def gameoverClick(self, event):
        self.boutonGameoverRejouer.checkIfButtonIsClicked(event)
        self.boutonGameoverQuitter.checkIfButtonIsClicked(event)

        if self.boutonGameoverRejouer.isClicked == 1:
            self.parent.rejouer()
            self.boutonGameoverRejouer.isClicked = 0
        elif self.boutonGameoverQuitter.isClicked == 1:
            self.root.destroy()

    def menuClick(self, event):
        self.boutonMenuJouer.checkIfButtonIsClicked(event)
        self.boutonMenuQuitter.checkIfButtonIsClicked(event)

        if self.boutonMenuJouer.isClicked == 1:
            self.afficherterrain()
            self.boutonMenuJouer.isClicked = 0
        elif self.boutonMenuQuitter.isClicked == 1:
            self.root.destroy()

class ChoixTour:
    def __init__(self, object):
        self.isEnabled=0
        self.object=object
        self.cercle=None
        self.option1=None
        self.option2=None
        self.option3=None
        self.option4=None
        self.option5=None

        self.tourTmp=None

        self.imgArcher=ImageTk.PhotoImage(Image.open("assets/archer.gif"))
        self.imgMagic=ImageTk.PhotoImage(Image.open("assets/magic.gif"))
        self.imgLazer=ImageTk.PhotoImage(Image.open("assets/lazer.gif"))
        self.imgBomb=ImageTk.PhotoImage(Image.open("assets/bomb.gif"))

    def afficheChoix(self, t):
        self.diametre = 55
        isEnabled = 0
        self.nombreOption= 4
        self.espacement = 360 / self.nombreOption

        self.cercle = self.object.create_oval(t.posX-self.diametre, t.posY-self.diametre, t.posX+self.diametre, t.posY+self.diametre, fill="", width=2)

        x,y = helper.Helper.getAngledPointRadian(45,self.diametre,t.posX,t.posY)
        self.option1= self.object.create_image(x,y,image = self.imgArcher, tag="imgArcher")

        x,y = helper.Helper.getAngledPointRadian(45 + self.espacement*1,self.diametre,t.posX,t.posY)
        self.option2= self.object.create_image(x,y,image = self.imgMagic, tag="imgMagic")

        x,y = helper.Helper.getAngledPointRadian(45 + self.espacement*2,self.diametre,t.posX,t.posY)
        self.option3= self.object.create_image(x,y,image = self.imgLazer, tag="imgLazer")

        x,y = helper.Helper.getAngledPointRadian(45 + self.espacement*3,self.diametre,t.posX,t.posY)
        self.option4= self.object.create_image(x,y,image = self.imgBomb, tag="imgBomb")     

    def supprimeChoix(self, t):
        self.object.delete(self.cercle)
        self.object.delete(self.option1)
        self.object.delete(self.option2)
        self.object.delete(self.option3)
        self.object.delete(self.option4)
        self.object.delete(self.option5)

class BoutonArtisanal:
    def __init__(self,parent, width, height, color, itemID):
        self.parent=parent
        self.x1=0
        self.y1=0
        self.x2=width
        self.y2=height
        self.width=width
        self.height=height
        self.color=color
        self.rect=None
        self.text=None
        self.fontTaille=40
        self.isClicked=0
        self.itemID=itemID

    def create_button_box(self, objet):
        self.rect = objet.create_rectangle(self.x1, self.y1, self.x2, self.y2, fill=self.color, width=0)

    def center_button(self, objet, yOffset):
        xOffset = (self.parent.largeurTerrain / 2) - (self.x2 - self.x1) / 2
        self.move_button(objet, xOffset, yOffset)

    def move_button(self, objet, xOffset, yOffset):
        objet.move(self.rect, xOffset, yOffset)
        coord = objet.bbox(self.rect)
        self.x1 = coord[0]
        self.y1 = coord[1]
        self.x2 = coord[2]
        self.y2 = coord[3]

    def create_normal_text(self, objet, myStr, taille, color, tags):
        objet.create_text((self.x1 + self.x2) /2, (self.y2 + self.y1) / 2,
                           text=myStr, font=('Times', taille, 'bold'), fill=color, tags=tags)

    def create_button_text(self, objet, myStr, color, tags):
        color='black'
        objet.create_text((self.x1 + self.x2) /2, (self.y2 + self.y1) / 2,
                           text=myStr, font=('Times', self.fontTaille, 'bold'), activefill='white', fill=color, tags=tags)

    def checkIfButtonIsClicked(self, event):
        self.x=event.x
        self.y=event.y

        if self.x > self.x1 and self.x < self.x2 and self.y > self.y1 and self.y < self.y2:
            self.isClicked=1

