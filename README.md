# TowerDefense

Small Tower Defense game using python with tkinter

## Getting Started

You can run such a script on IDE (Eclipse, Visual Studio Code).
Or you can run it from the command prompt on windows as describe bellow.

### Prerequisites

Make sure to have python 3 installed on your machine.
https://www.python.org/downloads/

### How-to-run:

Open Eclipse or Visual Code Studio
Execute towerDefense.py

or

Open cmd prompt

```
cd towerdefense
```
```
python towerDefense.py
```

or 

```
py -3 towerDefense.py
```

## Authors

* Henry Lin
* Antoine Girard Dubreuil
* Olivier Bousquet

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* jmd
* Inspiration
* etc

<!---*****************************************************

Dernier Sprint

Comment jouer:

Cliquer sur le carré pour selectionner une tour de notre choix.
Pour améliorer le tour, click sur la tour puis click sur le bouton améliorer en bas de écran.
Pour vendre le tour, click sur la tour puis click sur le bouton vendre en bas de écran.
Le héro est utilisé pour combattre le creep et inoculer un virus au Creep.
Pour deplacer le héro, cliquer le héro pour selectionner puis clique sur l'emplacement désirer.
Pour déselectionner le héro, cliquer le bouton droite.

Type de tour:
Arc à flèche: lance une flèche devastatrice
Magique: lance une boule magique
Bombarde: lance un bombe au Creep et endommager tout les Creeps autours
LazerBeam: lance un lazer à chaque 0.04 seconde 

*Bug

-Il y a peut-être des tours qui lance un projectile sur un terrain vide
-argent en négatif

-->