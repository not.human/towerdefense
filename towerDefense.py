#***************************************#
# Auteurs: Henry Lin,					#
#		   Olivier Bousquet				#
#		   Antoine Girard Dubreuil		#
#										#
# Projet : Jeu Tower Defense			#
# Nom :    Land of Conqueror			#
#										#
#***************************************#

#-*- coding: utf-8 -*-
import Vue
import helper
import data

class Minion:
	def __init__(self,parent,vie,attack,armor,vitesse,detectionRange):
		self.parent=parent
		self.etat="idle"
		self.vie=vie
		self.attack=attack
		self.armor=armor
		self.vitesse=vitesse
		self.detectionRange=detectionRange
		
class Hero:
	def __init__(self,parent,lv,vie,attack,armor,vitesse,detectionRange):
		self.parent=parent
		self.etat="idle"
		self.lv=lv
		self.vie=vie+lv*50
		self.attack=attack+lv*1
		self.armor=armor+lv*1
		self.vitesse=vitesse
		self.detectionRange=detectionRange
		self.attSpeed=40
		self.cd=0
		self.attacked=0

		self.x=400 #centre du map
		self.y=50 #centre du map
		self.cibleX=400 #pour qu'il ne bouge pas
		self.cibleY=300
		self.angle=None

		self.isSelected=0
		
	def deplace(self):
		self.angle=helper.Helper.calcAngle(self.x, self.y, self.cibleX, self.cibleY)
		self.x,self.y=helper.Helper.getAngledPoint(self.angle,self.vitesse,self.x,self.y)
		distance = helper.Helper.calcDistance(self.x,self.y,self.cibleX,self.cibleY)
		if distance <= self.vitesse:
			self.etat="idle"
		
class Map:
	def __init__(self,parent, map):
		self.parent=parent

		if map == "map1":
			self.courbeXY = data.courbeXY
			self.tempsPourProchainVague=data.tempsPourProchainVague[0]
			self.nbCreepSelonVagues=data.nbCreepSelonVagues[0]
			self.terrainReserveePourTourXY=data.terrainReserve1

		if map == "map2":
			self.courbeXY = data.courbeXY2Depart1
			self.tempsPourProchainVague=data.tempsPourProchainVague[1]
			self.nbCreepSelonVagues=data.nbCreepSelonVagues[1]
			self.terrainReserveePourTourXY=data.terrainReserve2
		
class Tour:
	def __init__(self, typeDeTour, niveau, posX, poxY, idTour):
		self.typeDeTour=typeDeTour
		self.niveau=niveau
		if self.typeDeTour == "Archer":
			tower=data.archer[0]
		elif self.typeDeTour == "Magique":
			tower=data.magique[0]
		elif self.typeDeTour == "Bombarde":
			tower=data.bombarde[0]
		elif self.typeDeTour == "LazerBeam":
			tower=data.lazerBeam[0]

		self.cout=int(tower[2])
		self.collision=int(tower[3]) #collision = range
		self.attaque=int(tower[4])
		self.attSpeed=int(tower[5])
		self.posX=posX
		self.posY=poxY
		self.isEnabled=0
		self.idTour=idTour
		self.creepDansLeRange=0
		self.target=None
		self.fired=0
		self.cd=0
		self.coutTotal=self.cout
		
	def sell(self):
		self.rembourse = self.coutTotal * 0.75
		self.isEnabled=0
		return self.rembourse
	
	def ameliorer(self, typeDeTour, niveau):
		niveau+=1
		if typeDeTour == "Archer":
			tower=data.archer[niveau]
		elif typeDeTour == "Magique":
			tower=data.magique[niveau]
		elif typeDeTour == "Bombarde":
			tower=data.bombarde[niveau]
		elif typeDeTour == "LazerBeam":
			tower=data.lazerBeam[niveau]
		self.niveau=int(tower[1])
		self.cout=int(tower[2])
		self.collision=int(tower[3]) #collision = range
		self.attaque=int(tower[4])
		self.attSpeed=int(tower[5])
		self.coutTotal += self.cout
		return self.cout
		
class Creep:
	def __init__(self,parent,vie,attack,armor,vitesse,cash):
		#consulter le fichier excel(incomplete) pour les creeps
		self.parent=parent
		self.etat="standBy"
		self.vie=vie
		self.vieMax=vie
		self.vitesse=vitesse
		self.attack=attack
		self.armor=armor
		self.cash=cash
		self.courbeXY=self.parent.map.courbeXY
		self.x=self.courbeXY[0][0]
		self.y=self.courbeXY[0][1]
		self.cibleX = self.courbeXY[1][0]
		self.cibleY = self.courbeXY[1][1]

		self.angle=helper.Helper.calcAngle(self.x,self.y,self.cibleX,self.cibleY)
		self.position=0
		self.distanceEntreHeroEtCreep=0
		self.cibleXtemp=0 	#utiliser pour garder le cible original avant le combat
		self.cibleYtemp=0 	#utiliser pour garder le cibel original avant le combat
		self.pourcentageVie=1
		self.cible=None
		
	def deplace(self):
		if self.etat=="sorti":
			self.x,self.y=helper.Helper.getAngledPoint(self.angle,self.vitesse,self.x,self.y)
			distance = helper.Helper.calcDistance(self.x,self.y,self.cibleX,self.cibleY)
			#compare la distance entre le x,y et cibleX,cibleY au lieu de vérifier si x est égal à cibleX
				
			if distance <= self.vitesse:
				self.position += 1
				if self.position < len(self.courbeXY):
					self.cibleX = self.courbeXY[self.position][0]
					self.cibleY = self.courbeXY[self.position][1]
					self.angle=helper.Helper.calcAngle(self.x,self.y,self.cibleX,self.cibleY)
				else:
					# mort en sortant du jeu
					self.etat=="mort"
					self.parent.diminuehp()
					self.parent.nombreDeCreepMort+=0.5
		elif self.etat=="combat":
			
			self.angle=helper.Helper.calcAngle(self.x,self.y,self.cibleXtemp,self.cibleYtemp)
			self.x,self.y=helper.Helper.getAngledPoint(self.angle,self.vitesse,self.x,self.y)
		else:
			# mort parce que vie == 0
			self.etat="mort"
			self.parent.ajouterargent()

class Projectile:
	def __init__(self, tower, target, temps):
		self.tower=tower
		self.x=tower.posX
		self.y=tower.posY
		self.cibleX,self.cibleY=helper.Helper.getAngledPoint(target.angle, target.vitesse * temps, target.x, target.y)
		self.angle=helper.Helper.calcAngle(self.x,self.y,self.cibleX,self.cibleY)
		self.vitesse = (helper.Helper.calcDistance(self.x,self.y,self.cibleX,self.cibleY)) / (temps)
		self.target=target

	def deplace(self):
		self.x,self.y=helper.Helper.getAngledPoint(self.angle,self.vitesse,self.x,self.y)
		
class JeuTowerDefence:
	def __init__(self,parent, maps):
		self.parent=parent
		self.largeur=800
		self.hauteur=600
		self.mod=None
		self.map=maps
		self.creeps=[] # creep préparer pour le niveau
		self.creepsSorti=[]	# creep visible dans le jeu
		self.cash=200
		self.hp=15
		self.delay=10 		# temps necessaire pour que un autre creep entre dans le jeu
		self.tempsEcouler=0 # temps ecouler depuis le début du jeu
		self.nbVagues=0;
		self.projectiles=[]
		self.tours=[]
		self.hero=[]
		self.creerHero()
		self.nombreDeCreepMort=0
			
	def creerCreeps(self):
		nc=self.map.nbCreepSelonVagues[self.nbVagues]
		for i in range(nc):
			c = Creep(self,	int(data.creepType1[2]), int(data.creepType1[3]), float(data.creepType1[4]), float(data.creepType1[5]), float(data.creepType1[6]))
			self.creeps.append(c)
	
	def ajouterargent(self):
		self.cash+=6
				
	def jouercoup(self):
		self.tempsEcouler += 1
		morts=[]
		hit=[]
		if self.nbVagues < 5:
			if self.tempsEcouler ==self.map.tempsPourProchainVague[self.nbVagues]:
				self.creerCreeps()
				self.nbVagues +=1
		
		if self.tempsEcouler % self.delay == 0:
			for i in self.creeps:
				if i.etat=="standBy":
					i.etat = "sorti"
					self.creepsSorti.append(i)
					break
		
		for i in self.tours:
			for j in self.creepsSorti:
				if helper.Helper.calcDistance(i.posX,i.posY,j.x,j.y) < i.collision/2:
					i.creepDansLeRange = 1
					i.target = j
					break
				else:
					i.creepDansLeRange = 0
		#projectiles améliorer 
		for i in self.tours:
				#si tour est crée
				if(i.isEnabled == 1):
					if i.typeDeTour != "LazerBeam":
						#vérifier le cooldown du projectile
						if i.fired == 0:
							#vérifier si il y a un creeps dans le portée
							if i.creepDansLeRange == 1:
								if i.target != None:		
									p=Projectile(i , i.target, 10) #le projectile va toucher le creep dans 10 increment
									self.projectiles.append(p)
									i.fired = 1					
						else:
							if i.cd == i.attSpeed:
								i.fired = 0
								i.cd = 0
							else:
								i.cd +=1
					#cas si c'est un lazerBeam
					else:
						if i.target != None:
							p=Projectile(i, i.target, 2) # Un projectile invisible qui sera afficher comme une ligne
							self.projectiles.append(p)

		for i in self.projectiles:
			#projectiles améliorer depuis 25 mai
			distance = helper.Helper.calcDistance(i.x,i.y,i.cibleX,i.cibleY)
			if distance <= i.vitesse:
				hit.append(i)
				#le bombarde fait des dommage au autre creep
				if i.tower.typeDeTour == "Bombarde":
					for c in self.creepsSorti:
						#vérifier si les creeps sont proches de l'explosion
						if helper.Helper.calcDistance(i.x,i.y,c.x,c.y) <= 50:
							c.vie -= (i.tower.attaque - c.armor)
							c.pourcentageVie = (c.vie/c.vieMax)
				else:
					for j in self.creepsSorti:
						#trouver le creep qu'il devrait attaquer
						if i.target == j:
							if i.tower.typeDeTour == "Archer":
								j.vie -= (i.tower.attaque - j.armor)
							elif i.tower.typeDeTour == "Magique":
								j.vie -= (i.tower.attaque - j.armor)
							elif i.tower.typeDeTour == "LazerBeam":
								j.vie -= (i.tower.attaque - j.armor)			
			i.deplace()

		for i in self.creeps:	
			if i.etat=="sorti":
				self.creeps.remove(i)
				
		for i in self.creepsSorti:
			i.pourcentageVie = (i.vie/i.vieMax)
			if i.vie <= 0:
				i.etat="mort"
			if i.etat=="mort":
				morts.append(i)
			i.deplace()
		
		for i in hit:
			self.projectiles.remove(i)
					
		for i in morts:
			self.creepsSorti.remove(i)
			self.nombreDeCreepMort+=1

		if self.hero[0].etat=="deplace":
			self.hero[0].deplace()
		elif self.hero[0].etat=="idle":
			creepInRange=[]
			for j in self.creepsSorti:
				distance=helper.Helper.calcDistance(self.hero[0].x, self.hero[0].y, j.x, j.y)
				#vérifier si le creep est dans le portée du hero
				if distance < self.hero[0].detectionRange:
					j.distanceEntreHeroEtCreep = distance
					creepInRange.append(j.distanceEntreHeroEtCreep)
			
			#logique pour choisir le creep le plus proche du hero
			if creepInRange:
				for i in self.creepsSorti:
					if i.distanceEntreHeroEtCreep == min(creepInRange):
						self.hero[0].etat="combat"
						i.etat="combat"
						self.hero[0].cibleX=i.x #deplace vers la direction où le creep est mainteneant
						self.hero[0].cibleY=i.y #donc on change son cible
						self.hero[0].angle=helper.Helper.calcAngle(self.hero[0].x, self.hero[0].y, self.hero[0].cibleX, self.hero[0].cibleY)
						i.cibleXtemp = self.hero[0].x #même principe mais pour le creep
						i.cibleYtemp = self.hero[0].y #utilise cible temporaire pour pas perdre le cible original apres le combat
						break
		elif self.hero[0].etat=="combat":
			for i in self.creepsSorti:
				if i.etat=="combat": #verifier lequels des creeps est suppose d'etre en combat avec le hero
					distance=helper.Helper.calcDistance(self.hero[0].x,self.hero[0].y, i.x, i.y)
					#si les deux sont assez proche, donc combat commence et ne bouge PAS!!!!!!!!!!!!!!
					if distance <= 50:
						#attaque
						if self.hero[0].attacked == 0:
							if i.vie < 0:
								i.etat="mort"
								self.hero[0].etat="idle"
							elif self.hero[0].vie < 0:
								self.hero[0].etat="mort"
								i.etat="sorti"
							else:	
								i.vie-=self.hero[0].attack
								self.hero[0].vie-=1
								i.pourcentage = (i.vie/i.vieMax)
							self.hero[0].attacked = 1
						#une pause pour son prochain attaque
						elif self.hero[0].attacked == 1:
							if self.hero[0].cd == self.hero[0].attSpeed:
								self.hero[0].attacked = 0
								self.hero[0].cd = 0
							else:
								self.hero[0].cd+=1
					break
			self.hero[0].etat = "idle"
						

	def creerTour(self):
		idNumber=0
		for i in self.map.terrainReserveePourTourXY:
			t=Tour("Archer", 0, i[0], i[1],"tour"+str(idNumber))
			self.tours.append(t)
			idNumber+=1
		
	def diminuehp(self):
		self.hp-=0.5	

	def creerHero(self):
		h=Hero(self, 1, 100, 10, 2, 10, 100)
		self.hero.append(h)
	
	def deplaceHero(self,x,y,etat):
		self.hero[0].etat = etat
		self.hero[0].cibleX=x
		self.hero[0].cibleY=y

	def addTower(self, typeDeTour,niveau,posX,poxY,idTour):
		t=Tour(typeDeTour,niveau,posX,poxY,idTour)
		t.isEnabled=1
		self.tours.append(t)
		
	def sellTower(self, tourID):

		for t in self.tours:
			if t.idTour == tourID:
				self.cash += t.sell()
		
	def ameliorerTower(self, tourID):

		for t in self.tours:
			if t.idTour == tourID:
				self.cash -= t.ameliorer(t.typeDeTour,t.niveau)
				
		
class Controleur():
	def __init__(self):
		self.mod=JeuTowerDefence(self, Map(self, "map1"))
		self.vue=Vue.Vue(self)
		self.vue.afficherterrain()
		self.vue.affichermenu()
		self.mod.creerTour()
		self.vue.placerTour()
		#variable bool
		self.gameIsRunning
		
		self.vue.root.mainloop()
		
	def jouercoup(self):
		self.mod.jouercoup()
		
		if self.gameIsRunning == 1:
			self.vue.afficheEtatDuJeu()
			self.vue.root.after(40, self.jouercoup)

		if self.mod.hp == 0:
			self.vue.menuGameOver()
			self.gameIsRunning = 0

		if self.mod.nombreDeCreepMort >= 65 and self.mod.hp > 0: 
			self.vue.menuVictory()
			self.gameIsRunning = 0

	def rejouer(self):
		self.vue.canevas.delete("all")
		self.mod=JeuTowerDefence(self, Map(self,"map1"))
		self.vue.mod=self.mod
		self.vue.affichermap1()
		self.vue.afficherterrain()
		self.vue.placerTour()
		self.mod.creerTour()
		self.mod.jouercoup()

	def continuer(self):
		self.vue.canevas.delete("all")
		self.mod=JeuTowerDefence(self, Map(self,"map2"))
		self.vue.mod=self.mod
		self.vue.affichermap2()
		self.vue.afficherterrain()
		self.vue.placerTour()
		self.mod.creerTour()
		self.mod.jouercoup()

	def pauseGame(self):
		if self.gameIsRunning == 1:
			self.gameIsRunning = 0
		else:
			self.gameIsRunning = 1

		self.jouercoup()

if __name__ == '__main__':
	c=Controleur()