import re

###########################################
temp=open("data/Creeps.txt")
creepStat = temp.readlines()
creepType1 = creepStat[1].split(";")
creepType2 = creepStat[2].split(";")
creepType3 = creepStat[3].split(";")
creepType4 = creepStat[4].split(";")
temp.close

###########################################
temp=open("data/Map.txt")
courbeXY=[]
mapCourbe = temp.readlines();

mapCourbe1 = mapCourbe[1].split(';')

for elem in mapCourbe1:
	if re.match('^[0-9]+,[0-9]+$', elem):
		x,y = elem.split(',')
		courbeXY.append((int(x),int(y)))

courbeXY2Depart1=[]
courbeXY2Depart2=[]
mapCourbe2Depart1 = mapCourbe[2].split(';')
mapCourbe2Depart2 = mapCourbe[3].split(';')

for elem in mapCourbe2Depart1:
	if re.match('^[0-9]+,[0-9]+$', elem):
		x,y = elem.split(',')
		courbeXY2Depart1.append((int(x),int(y)))

for elem in mapCourbe2Depart2:
	if re.match('^[0-9]+,[0-9]+$', elem):
		x,y = elem.split(',')
		courbeXY2Depart2.append((int(x),int(y)))
temp.close()

###########################################
temp=open("data/TerrainReserve.txt")
ligne = temp.readlines();

terrainReserve1=[]
terrainReserve2=[]

mapTerrainReserve1 = ligne[1].split(';')
mapTerrainReserve2 = ligne[2].split(';')

for elem in mapTerrainReserve1:
	if re.match('^[0-9]+,[0-9]+$', elem):
		x,y = elem.split(',')
		terrainReserve1.append((int(x),int(y)))

for elem in mapTerrainReserve2:
	if re.match('^[0-9]+,[0-9]+$', elem):
		x,y = elem.split(',')
		terrainReserve2.append((int(x),int(y)))



###########################################
temp=open("data/Map-suite.txt")
ligne = temp.readlines();

tempsPourProchainVague=[]
nbCreepSelonVagues=[]

mapTempProchaineVague = ligne[0].split(';')
mapNbCreepSelonVagues = ligne[1].split(';')

for elem in mapTempProchaineVague:
	if re.match('^[0-9]', elem):
		value=elem.split(',')

		value = list(map(int, value))

		tempsPourProchainVague.append(value)


for elem in mapNbCreepSelonVagues:
	if re.match('^[0-9]', elem):
		value=elem.split(',')

		value = list(map(int, value))

		nbCreepSelonVagues.append(value)

###########################################

temp=open("data/Tours.txt")
ligne=temp.readlines()
archer0=ligne[1].split(";")
archer1=ligne[2].split(";")
archer2A=ligne[3].split(";")
archer2B=ligne[4].split(";")

archer =[archer0, archer1, archer2A, archer2B]

magique0=ligne[6].split(";")
magique1=ligne[7].split(";")
magique2A=ligne[8].split(";")
magique2B=ligne[9].split(";")

magique =[magique0, magique1, magique2A, magique2B]

bombarde0=ligne[11].split(";")
bombarde1=ligne[12].split(";")
bombarde2A=ligne[13].split(";")
bombarde2B=ligne[14].split(";")

bombarde =[bombarde0, bombarde1, bombarde2A, bombarde2B]

lazerBeam0=ligne[16].split(";")
lazerBeam1=ligne[17].split(";")
lazerBeam2A=ligne[18].split(";")
lazerBeam2B=ligne[19].split(";")

lazerBeam =[lazerBeam0, lazerBeam1, lazerBeam2A, lazerBeam2B]
temp.close()

###########################################

temp=open("data/Vague.txt")
ligne=temp.readlines()
vagues1=ligne[1].split(";")
vagues2=ligne[2].split(";")
vagues3=ligne[3].split(";")
vagues4=ligne[4].split(";")
vagues5=ligne[5].split(";")

vagues =[vagues1, vagues2, vagues3, vagues4, vagues5]

temp.close()

